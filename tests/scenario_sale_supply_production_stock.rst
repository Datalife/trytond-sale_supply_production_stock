=====================================
Sale Supply Production Stock Scenario
=====================================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import config, Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.tests.tools import activate_modules
    >>> today = datetime.date.today()

Install sale_supply_production_stock::

    >>> config = activate_modules('sale_supply_production_stock')


Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> payable = accounts['payable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> account_tax = accounts['tax']
    >>> account_cash = accounts['cash']

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Configuration production location::

    >>> Location = Model.get('stock.location')
    >>> warehouse, = Location.find([('code', '=', 'WH')])
    >>> production_location, = Location.find([('code', '=', 'PROD')])
    >>> warehouse.production_location = production_location
    >>> warehouse.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.producible = True
    >>> template.salable = True
    >>> template.producible = True
    >>> template.list_price = Decimal(30)
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal(20)
    >>> product.save()

Create Components::

    >>> component1 = Product()
    >>> template1 = ProductTemplate()
    >>> template1.name = 'component 1'
    >>> template1.default_uom = unit
    >>> template1.producible =True
    >>> template1.type = 'goods'
    >>> template1.list_price = Decimal(5)
    >>> template1.save()
    >>> component1.template = template1
    >>> component1.cost_price = Decimal(1)
    >>> component1.save()

    >>> component2 = Product()
    >>> template2 = ProductTemplate()
    >>> template2.producible = False
    >>> template2.name = 'component 2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.list_price = Decimal(7)
    >>> template2.save()
    >>> component2.template = template2
    >>> component2.cost_price = Decimal(5)
    >>> component2.save()

Create Bill of Material with Component 1::

    >>> BOM = Model.get('production.bom')
    >>> BOMInput = Model.get('production.bom.input')
    >>> BOMOutput = Model.get('production.bom.output')
    >>> ProductBom = Model.get('product.product-production.bom')

    >>> component1_bom = BOM(name='product')
    >>> input1 = BOMInput()
    >>> component1_bom.inputs.append(input1)
    >>> input1.product = component1
    >>> input1.quantity = 1

    >>> output = BOMOutput()
    >>> component1_bom.outputs.append(output)
    >>> output.product = product
    >>> output.quantity = 1
    >>> component1_bom.save()

    >>> product.boms.append(ProductBom(bom=component1_bom, sequence=1))
    >>> product.save()

Create Bill of Material with component 2::

    >>> BOM = Model.get('production.bom')
    >>> BOMInput = Model.get('production.bom.input')
    >>> BOMOutput = Model.get('production.bom.output')
    >>> bom = BOM(name='product')
    >>> input1 = BOMInput()
    >>> bom.inputs.append(input1)
    >>> input1.product = component2
    >>> input1.quantity = 1
    >>> output = BOMOutput()
    >>> bom.outputs.append(output)
    >>> output.product = product
    >>> output.quantity = 1
    >>> bom.save()

    >>> product.boms.append(ProductBom(bom=bom, sequence=2))
    >>> product.save()

Sale product without inventory::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale.sale_date = today + relativedelta(days=-5)
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 2.0
    >>> sale.save()
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> sale.reload()

Production must be supplied with sale line quantity::

    >>> bool(sale.lines[0].supply_production)
    True
    >>> sale.lines[0].remaining_stock
    -2.0
    >>> shipment, = sale.shipments
    >>> shipment.effective_date = today + relativedelta(days=-4)
    >>> shipment.click('assign_try')
    False
    >>> shipment.click('assign_force')
    >>> shipment.click('pack')
    >>> shipment.click('done')
    >>> len(sale.productions)
    0

Create an Inventory::

    >>> Inventory = Model.get('stock.inventory')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory.effective_date = today + relativedelta(days=-3)
    >>> inventory.save()
    >>> inventory_line = inventory.lines.new(product=product)
    >>> inventory_line.quantity = 100.0
    >>> inventory_line = inventory.lines.new(product=component1)
    >>> inventory_line.quantity = 80.0
    >>> inventory_line = inventory.lines.new(product=component2)
    >>> inventory_line.quantity = 40.0
    >>> inventory.click('confirm')
    >>> sorted(m.quantity for l in inventory.lines for m in l.moves)
    [40.0, 80.0, 102.0]

Sale products with enough stock::

    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale.sale_date = today
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 75.0
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> sale.reload()
    >>> shipment, = sale.shipments
    >>> shipment.effective_date = today + relativedelta(days=-2)
    >>> shipment.click('assign_try')
    True
    >>> shipment.click('pack')
    >>> shipment.click('done')

Production must not be supplied::

    >>> bool(sale.lines[0].supply_production)
    False
    >>> len(sale.productions) == 0
    True

Ensure no productions created::

    >>> Production = Model.get('production')
    >>> Production.find([])
    []

Sale products without enough stock::

    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale.sale_date = today
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 100.0
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> sale.reload()
    >>> shipment, = sale.shipments
    >>> shipment.effective_date = today
    >>> shipment.click('assign_force')
    >>> shipment.click('pack')
    >>> shipment.click('done')

Productions are created in cascade with the needed stock in assigned state::

    >>> bool(sale.lines[0].supply_production)
    True
    >>> sale.lines[0].remaining_stock
    -75.0
    >>> production, = sale.productions
    >>> production.bom == product.boms[0].bom
    True
    >>> production.quantity
    75.0
    >>> production.outputs[0].quantity
    75.0
    >>> production.state
    'done'
    >>> len(Production.find([]))
    1

Sale products again consuming 2 boms::

    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale.sale_date = today
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 60.0
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> shipment, = sale.shipments
    >>> shipment.effective_date = today
    >>> shipment.click('assign_force')
    >>> shipment.click('pack')
    >>> shipment.click('done')
    >>> sale.reload()
    >>> bool(sale.lines[0].supply_production)
    True
    >>> sale.lines[0].remaining_stock
    -60.0
    >>> productions = sale.productions
    >>> len(productions)
    2
    >>> production1, = [p for p in productions if p.bom == product.boms[0].bom]
    >>> production1.quantity
    5.0
    >>> production2, = [p for p in productions if p.bom == product.boms[1].bom]
    >>> production2.quantity
    40.0
    >>> [p.state for p in productions]
    ['done', 'done']